module Main where

import Lib
import ADT

main :: IO ()
main = print "Hello, World"

fact = Def "factorial" (Func
                            [Symb "n"]
                            [Symb "if", List [Symb "eq?", Symb "n", Number 1], 
                                        Number 1,
                                        List [Symb "*", Symb "n", Symb "factorial", List [Symb "-", Symb "n", Number 1]]
                            ]
                            [{-primitives...-}]
                       )
                       
fib = Def "fib" (Func
                     [Symb "n"]
                     [Symb "if", List [Symb "<=", Symb "n", Number 2],
                                 Number 1,
                                 List [Symb "+", List [Symb "fib", List [Symb "-", Symb "n", Number 1]], 
                                                 List [Symb "fib", List [Symb "-", Symb "n", Number 2]]
                                      ]
                     ]
                     [{-primitives...-}]
                )
                     
