module ADT where

data Expr = Symb String
          | Number Integer
          | String String
          | List [Expr]
          | Cons Expr Expr
          | Prim ([Expr] -> Expr)
          | Def String Expr
          | Func [Expr] [Expr] [(String, Expr)]
          | Macro [Expr] [Expr] [(String, Expr)]

{-         
primitives :: [(String, Expr)]
primitives = [ ("+", Prim $ numBinOp (+)),
               ("-", Prim $ numBinOp (-)),
               ("*", Prim $ numBinOp (*)),
               ("/", Prim $ numBinOp (/)),
               ("modulo", Prim $ numBinOp (mod)),
               ("remainder", Prim $ numBinOp (rem)),
               ("quotient", Prim $ numBinOp (quot)),
               ("=", Prim $ numEqOp (==)),
               ("eq?", Prim $ numEqOp (==)),
               ("<", Prim $ numCompOp (<)),
               (">", Prim $ numCompOp (>)),
               (">=", Prim $ numCompOp (>=)),
               ("<=", Prim $ numCompOp (<=)),
               ("and", Prim $ boolBinOp (&&)),
               ("or", Prim $ boolBinOp (||)),
               ("not", Prim $ boolUnOp (not)),
               ("car", Prim $ carOp),
               ("cdr", Prim $ cdrOp),
               ("list", Prim $ listOp)
             ]    
-}