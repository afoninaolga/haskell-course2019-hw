{-# LANGUAGE QuasiQuotes, OverloadedStrings #-}

module Main (main) where

import Text.Megaparsec
import Text.InterpolatedString.QM (qnb)
import Text.Pretty.Simple (pPrint)
import Data.Text (Text)
--import Lib
import Hawk
import Parser
--type Parser = Parsec Void Text



main :: IO ()
main = do
  parseTest (symbol "{") "{  "
  pPrint $ parseMaybe whileParser test
  parseTest whileParser test
    where
        test = [qnb|
        function sum(a, b) {
            return a + b;
        }
        BEGIN {
          a = 42;
          pow = 1;
          while (a > 1) {
            a = a / 2;
            pow = pow + 1;
          }
          print pow;
          x = (1 + (2 * 3)) "4";
          arr[x] = 1;
        }
        #for each line
        {  OFS = ";";
          if (NF > 2) {
            print $1, $2, $3;
          } else {
            print NF;
          }
        }
        
        END {
          var = "var";
          print "any string", var, sum(a, b), 1.23
        }
        |]

