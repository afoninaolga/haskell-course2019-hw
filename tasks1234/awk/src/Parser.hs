module Parser where

import Hawk
import Text.Megaparsec
import Text.Megaparsec.Char
import Control.Monad (void)
import Control.Monad.Combinators.Expr
import qualified Text.Megaparsec.Char.Lexer as L
import Data.Void

type Parser = Parsec Void String

sc ::Parser ()
sc = L.space space1 (L.skipLineComment "#") empty

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

integer :: Parser Integer
integer = L.signed sc (lexeme L.decimal)

double :: Parser Double
double = L.signed sc (lexeme L.float)

stringLiteral :: Parser String
stringLiteral = char '"' >> manyTill L.charLiteral (char '"')

parseNR :: Parser AValue
parseNR = do
  rword "NR"
  return NR  

parseNF :: Parser AValue
parseNF = do
  rword "NF"
  return NF

parseFS :: Parser AValue
parseFS = do
  rword "FS"
  return FS

parseOFS :: Parser AValue
parseOFS = do
  rword "OFS"
  return OFS

parseRS :: Parser AValue
parseRS = do
  rword "RS"
  return RS

parseORS :: Parser AValue
parseORS = do
  rword "ORS"
  return ORS

parsePosVar :: Parser AValue
parsePosVar = do
  char '$'
  value <- integer
  return PosVar{position = value}

symbol :: String -> Parser String
symbol = L.symbol sc

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

brackets :: Parser a -> Parser a
brackets = between (symbol "[") (symbol "]")

semi :: Parser String
semi = symbol ";"

comma :: Parser String
comma = symbol ","

whileParser :: Parser Stmt
whileParser = between sc eof stmt

aOperators :: [[Operator Parser Expr]]
aOperators =
  [ [Prefix (Neg <$ symbol "-")]

  , [ InfixL (BExpr Mul <$ symbol "*")
    , InfixL (BExpr Div <$ symbol "/")
    , InfixL (BExpr Mod <$ symbol "%")
    ]

  , [ InfixL (BExpr Add <$ symbol "+")
    , InfixL (BExpr Subt <$ symbol "-")
    ]
    
  , [ InfixL (BExpr Concat <$ space)]

  , [ InfixN (RExpr GE <$ symbol ">=") 
      , InfixN (RExpr Gr <$ symbol ">")
      , InfixN (RExpr LE <$ symbol "<=")
      , InfixN (RExpr Less <$ symbol "<")
      , InfixN (RExpr Eq <$ symbol "==")
      , InfixN (RExpr NotEq <$ symbol "!=") ]

  ]

term :: Parser Expr
term = parens expr
    <|> Literal <$> parseNR
    <|> Literal <$> parseNF
    <|> Literal <$> parseRS
    <|> Literal <$> parseFS
    <|> Literal <$> parseORS
    <|> Literal <$> parseOFS
    <|> Literal <$> parsePosVar
    <|> try (IndVar <$> parseIndVar)
    <|> Var <$> identifier
    <|> Literal <$> VDouble <$> try double
    <|> Literal <$> VInt <$> integer
    <|> Literal <$> VString <$> stringLiteral

parseIndVar :: Parser (String, Expr)
parseIndVar = do
  name <- identifier
  index <- brackets expr
  return (name, index)

whileStmt :: Parser Stmt
whileStmt = do
  rword "while"
  cond <- parens expr
  body <- (symbol "{") *> stmt
  return While {cond = cond, body = body}

ifStmt :: Parser  Stmt
ifStmt = do
  rword "if"
  cond <- parens expr
  trueCase <- (symbol "{") *> stmt
  void (symbol "}")
  rword "else"
  falseCase <- (symbol "{") *> stmt
  return If {cond = cond, trueCase = trueCase, falseCase = falseCase}

assignStmt :: Parser Stmt
assignStmt = do
  name <- identifier
  void (symbol "=")
  value <- expr
  return VarAssign {name = name, value = value}

indAssignStmt :: Parser Stmt
indAssignStmt = do
  name <- identifier
  index <- brackets expr
  void (symbol "=")
  value <- expr
  return IndVarAssign {name = name, index = index, value = value}
 

declStmt :: Parser Stmt
declStmt = do
  rword "function"
  name <- identifier
  args <- parens $ identifier `sepBy` comma
  body <- (symbol "{") *> stmt
  return FuncDeclaration {name = name, argNames = args, body = body} 

call :: Parser Expr
call = do
  name <- identifier
  args <- parens $ expr `sepBy` comma
  return FCall {funcName = name, funcArgs = args}

callStmt :: Parser Stmt
callStmt = do
  name <- identifier
  args <- parens $ expr `sepBy` comma
  return FuncCall {name = name, args = args}


returnStmt :: Parser Stmt
returnStmt = do
  rword "return"
  value <- expr
  return Return {value = value}

printStmt :: Parser Stmt
printStmt = do
  rword "print"
  vals <- expr `sepBy` comma
  return Print {valsToPrint = vals}

assignFS :: Parser Stmt
assignFS = do
  rword "FS"
  void (symbol "=")
  value <- stringLiteral
  return FSAssign {valueFS = value}

assignRS :: Parser Stmt
assignRS = do
  rword "RS"
  void (symbol "=")
  value <- stringLiteral
  return RSAssign {valueRS = value}

assignOFS :: Parser Stmt
assignOFS = do
  rword "OFS"
  void (symbol "=")
  value <- stringLiteral
  return OFSAssign {valueOFS = value}

assignORS :: Parser Stmt
assignORS = do
  rword "ORS"
  void (symbol "=")
  value <- stringLiteral
  return ORSAssign {valueORS = value}

parseBegin :: Parser Stmt
parseBegin = do
  rword "BEGIN"
  stmts <- (symbol "{") *> stmt
  return BeginSection{beforeAll = stmts}

parseMiddle :: Parser Stmt
parseMiddle = do
  stmts <- (symbol "{") *> stmt
  return MiddleSection{forLines = stmts}

parseEnd :: Parser Stmt
parseEnd = do
  rword "END"
  stmts <- (symbol "{") *> stmt
  return EndSection{afterAll = stmts}

  
stmt :: Parser Stmt
stmt = f <$> sepEndBy1 stmt' (semi <|> (symbol "}"))
  where 
    f l = if length l == 1 then head l else Seq l

stmt' :: Parser Stmt
stmt' = try declStmt
     <|> try parseBegin
     <|> try parseMiddle
     <|> try parseEnd
     <|> try ifStmt
     <|> try whileStmt
     <|> try assignOFS
     <|> try assignORS
     <|> try assignFS
     <|> try assignRS
     <|> try indAssignStmt
     <|> try assignStmt
     <|> try callStmt
     <|> try returnStmt
     <|> try printStmt
     <|> parens stmt

expr' :: Parser Expr
expr' = makeExprParser term aOperators

expr :: Parser Expr
expr = try call <|> expr'

rword :: String -> Parser ()
rword w = (lexeme . try) (string w *> notFollowedBy alphaNumChar)
 
rws :: [String] -- list of reserved words
rws =
  [ "if"
  , "else"
  , "while"
  , "for"
  , "function"
  , "print"
  , "NR"
  , "NF"
  , "FS"
  , "RS"
  , "OFS"
  , "ORS"
  ]

identifier :: Parser String
identifier = (lexeme . try) (p >>= check)
  where
    p = (:) <$> (letterChar <|> char '_') <*> many (alphaNumChar <|> char '_')
    check x = if x `elem` rws
      then fail $ "keyword " ++ show x ++ " cannot be an identifier"
      else return x

