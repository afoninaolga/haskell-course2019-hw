module Main where

import RefalAst

{-
    Factorial {
        0 = 1;
        s.N = <Mul s.N <Factorial <Sub s.N 1>>>;
    }
-}

refalFactorial :: Declaration
refalFactorial = Declaration "Factorial" $ Block [
        Expression [Term $ Pattern $ Common $ Number 0] := Expression [Term $ General $ Common $ Number 1],
        Expression [Term $ Variable SymbolType "N"] := Expression [Term $ Call "Mul" $ Expression [
                Term $ General $ Value SymbolType "N", Term $ Call "Factorial" $ Expression [
                    Term $ Call "Sub" $ Expression [Term $ General $ Value SymbolType "N", Term $ General $ Common $ Number 1]
                ]
            ]
        ]
    ]

{-
    Palindrome {
        = True;
        s.1 = True;
        s.1 e.2 s.1 = <Palindrome e.2>;
        e.1 = False;
    }
-}

refalPalindrome :: Declaration
refalPalindrome = Declaration "Palindrome" $ Block [
        Expression [] := Expression [Term $ General $ Common $ Identifier "True"],
        Expression [Term $ Variable SymbolType "1"] := Expression [Term $ General $ Common $ Identifier "True"],
        Expression [Term $ Variable SymbolType "1", Term $ Variable ExpressionType "2", Term $ Pattern $ Value SymbolType "1"] :=
            Expression [Term $ Call "Palindrome" $ Expression [Term $ General $ Value ExpressionType "2"]],
        Expression [Term $ Variable ExpressionType "1"] := Expression [Term $ General $ Common $ Identifier "False"]
    ]

main :: IO ()
main = putStrLn "(~OvO)~"
