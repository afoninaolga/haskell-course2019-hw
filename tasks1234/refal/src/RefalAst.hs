module RefalAst where

data Program = Program [Declaration]

data Declaration = Declaration String Block

data Block = Block [Sentence]

infixl 1 :=

data Sentence = Expression Pattern := Expression General

data Expression a = Expression [Term a]

data Term a = Term a
    | Structure (Expression a)

data Common = Common Symbol
    | Value Type String

data Pattern = Pattern Common
    | Variable Type String

data General = General Common
    | Call String (Expression General)

data Symbol = Character Char
    | Identifier String
    | Number Integer
    | RealNumber Double 

data Type = SymbolType | TermType | ExpressionType
