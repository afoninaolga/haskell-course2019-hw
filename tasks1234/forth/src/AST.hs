module AST where

type Stack = [Integer]
type Dictionary = [(String, [Expr])]

data Expr = 
        Push Integer
      | Word String
      | BinOp (Stack -> Stack)
      | Cond [Expr] [Expr]
      | Loop [Expr]
      | Def String [Expr]

-- function getting a binary operaton on Stack from haskell operator
getBinOp :: (Integer -> Integer -> Integer) -> Stack -> Stack
getBinOp f (x:y:xs) = (f x y) : xs 
getBinOp _ _ = undefined

instance Show Expr where
  show (Push a) = "Push " ++ show a
  show (BinOp _) = "BinOp "
  show (Word a) = show a
  show (Loop a) = "Loop " ++ show a
  show (Cond a b) = "If " ++ show a ++ " Else " ++ show b
  show (Def a b) = "Define " ++ show a ++ " Is " ++ show b