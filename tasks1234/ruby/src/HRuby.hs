module HRuby where

data Type = BOOL | INT | CHAR | FLOAT | ARRAY | OBJECT | VOID | FUNCTION
    deriving (Show, Eq)

data RValue = VBool { valueBool::Bool } 
            | VInt { valueInt::Int }
            | VChar { valueChar::Char } 
            | VFloat { valueFloat::Float }
            | VArray { valueArray::[RValue] }
            | VFunction {  }
            | VObject { ofClass::String, fields::[RValue] }
            | VVoid
    deriving (Show)

getValueType::RValue -> Type 
getValueType (VBool _) = BOOL
getValueType (VInt _) = INT
getValueType (VChar _) = CHAR
getValueType (VFloat _) = FLOAT
getValueType (VArray _) = ARRAY
getValueType VVoid = VOID

data RCLass = RClass { name::String, mems::[Binding] }

infixl 6 #+ 
(#+)::RValue -> RValue -> RValue
(#+) (VBool a) (VBool b) = VBool (a || b)
(#+) (VInt a) (VInt b) = VInt (a + b)
(#+) (VFloat a) (VFloat b) = VFloat (a + b)
(#+) (VArray a) (VArray b) = VArray (a ++ b)
(#+) _ _ = undefined

infixl 6 #-
(#-)::RValue -> RValue -> RValue
(#-) (VInt a) (VInt b) = VInt (a - b)
(#-) (VFloat a) (VFloat b) = VFloat (a - b)
(#-) _ _ = undefined

infixl 7 #*
(#*)::RValue -> RValue -> RValue
(#*) (VBool a) (VBool b) = VBool (a && b)
(#*) (VInt a) (VInt b) = VInt (a * b)
(#*) (VFloat a) (VFloat b) = VFloat (a * b)
(#*) _ _ = undefined

infixl 7 #/
(#/)::RValue -> RValue -> RValue
(#/) (VInt a) (VInt b) = VInt (a `div` b)
(#/) (VFloat a) (VFloat b) = VFloat (a / b)
(#/) _ _ = undefined

rNegate::RValue -> RValue 
rNegate (VBool a) = VBool (not a)
rNegate (VInt a) = VInt (-a)
rNegate (VFloat a) = VFloat (-a)
rNegate _ = undefined

data Binding = FunBinding  {funName::String, parameters::[String], body::[Instruction] }
             | ClassBinding { className::String, members::[Binding] }
             | VarBinding { varName::String, rValue::RValue } 
             | Breaker
    deriving (Show)

type Context = [Binding]

lookupVar::Context -> String -> RValue
lookupVar [] name = error "Variable not in scope => NOSCOPE360MLG"
lookupVar (bnd@(VarBinding vn vl) : ctx) name = if (vn == name) then vl else lookupVar ctx name
lookupVar (bnd:ctx) name = lookupVar ctx name

data Expression = Val RValue 
                | Expression :+: Expression 
                | Expression :*: Expression 
                | Expression :/: Expression 
                | Expression :-: Expression
                | Neg Expression 
                | Var String 
                | Fun String [Expression]
    deriving (Show)


eval::Context -> Expression -> RValue
eval ctx (Neg e) = rNegate (eval ctx e)
eval ctx (Val x) = x
eval ctx (Var s) = lookupVar ctx s
eval ctx (e1 :+: e2) = eval ctx e1 #+ eval ctx e2
eval ctx (e1 :*: e2) = eval ctx e1 #* eval ctx e2
eval ctx (e1 :/: e2) = eval ctx e1 #/ eval ctx e2
eval ctx (e1 :-: e2) = eval ctx e1 #- eval ctx e2
eval ctx (Fun name args) = undefined


varBinding::Context -> String -> Expression -> Binding
varBinding ctx name expr = VarBinding {varName = name, rValue = val} where
    val = eval ctx expr

bindVar::Context -> Context -> String -> Expression -> Context -- add Breaker 
bindVar genCtx [] name expr = [varBinding genCtx name expr]
bindVar genCtx (Breaker:ctx) name expr = (varBinding genCtx name expr):Breaker:ctx
bindVar genCtx (bnd@(VarBinding vn _) : ctx) name expr = if (vn == name) then (varBinding genCtx name expr):ctx else bnd : (bindVar genCtx ctx name expr)
bindVar genCtx (bnd:ctx) name expr = bnd : (bindVar genCtx ctx name expr)


data Instruction = ForLoop { cntName::String, cntValues::[Int], forBody::[Instruction] } 
                 | WhileLoop { whileCond::Expression, whileBody::[Instruction] }
                 | If { ifCond::Expression, ifTrue::[Instruction], ifFalse::[Instruction] }
                 | VarAssign { newName::String, newValue::Expression }
                 | FuncDeclare { functionName::String, params::[String], funBody::[Instruction] }
                 | FuncCall { callName::String, args::[Expression] } 
                 | Ret { result::Expression }
                 | ClassDeclare { newClassName::String, newMembers::[Instruction] } -- eeeh
    deriving Show

-- \n Instruction needed if Tree is required

type Program = [Instruction]

executeProgram::Context -> Program -> Context

execute::Context -> Instruction -> Context
execute ctx wl@(WhileLoop {whileCond = cnd, whileBody = body}) = if valueBool (eval ctx cnd) then execute ctx' wl else ctx where
    ctx' = executeProgram ctx body
execute ctx (If { ifCond = cnd, ifTrue = bodyTrue, ifFalse = bodyFalse }) = if valueBool (eval ctx cnd) 
    then executeProgram ctx bodyTrue 
    else executeProgram ctx bodyFalse
execute ctx (VarAssign { newName = s, newValue = expr }) = bindVar ctx ctx s expr
execute ctx _ = undefined

executeProgram ctx [] = ctx
executeProgram ctx ((Ret expr):_) = (varBinding ctx "retValue" expr):(helper ctx) where
    helper::Context->Context
    helper [] = []
    helper (Breaker:bnds) = bnds
    helper (_:bnds) = helper bnds
executeProgram ctx (instruction:program) = executeProgram (execute ctx instruction) program